$(function () {
  var book = $("#book");
  var step = 0
  book.click(function () {
    if (window.innerWidth < 600) {
      return
    }

    var x = ++step % 3;

    switch (x) {
      case 0:
        book.removeClass().addClass("view-cover");
        break;
      case 1:
        book.removeClass().addClass("view-back");
        break;
      case 2:
        book.removeClass().addClass("open-book");
        break;

    }
    
  });

});
